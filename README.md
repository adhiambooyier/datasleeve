# Datasleeve

Final project for award of bachelors degree in computer science.
An android application for survey creation, data collection, presentation, and export for decision support.
This proposed application is projected to improve the efficiency of the field exercise by providing the researcher with various functionalities such as a tool to create the questionnaire within the application, edit the questions and responses in place as desired, and also record the responses electronically. These responses can then be exported in the desired formats which can finally be imported into an analytics software. 