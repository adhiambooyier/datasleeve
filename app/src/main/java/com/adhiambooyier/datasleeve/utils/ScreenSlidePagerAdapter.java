package com.adhiambooyier.datasleeve.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.adhiambooyier.datasleeve.fragments.AdminUserFragment;
import com.adhiambooyier.datasleeve.fragments.NewOrganizationFragment;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 2;

    public ScreenSlidePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AdminUserFragment();
            case 1:
                return new NewOrganizationFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }


}

