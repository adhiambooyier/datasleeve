package com.adhiambooyier.datasleeve.utils;

import com.adhiambooyier.datasleeve.models.NewUserRequest;
import com.adhiambooyier.datasleeve.models.Response;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {
    @POST("/")
    Call<Response> createNewUser(@Body NewUserRequest newUserRequest);
}
