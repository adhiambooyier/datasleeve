package com.adhiambooyier.datasleeve.utils;

public interface ViewPagerSwipeInterface {
    void onFragmentVisible();
}
