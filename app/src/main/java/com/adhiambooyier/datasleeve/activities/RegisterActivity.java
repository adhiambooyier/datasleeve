package com.adhiambooyier.datasleeve.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.fragments.AdminUserFragment;
import com.adhiambooyier.datasleeve.fragments.NewOrganizationFragment;
import com.adhiambooyier.datasleeve.utils.ScreenSlidePagerAdapter;
import com.adhiambooyier.datasleeve.utils.ViewPagerSwipeInterface;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    private ViewPager mPager;

    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(AdminUserFragment.newInstance());
        fragments.add(NewOrganizationFragment.newInstance());

        mPager = findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                ViewPagerSwipeInterface fragment = (ViewPagerSwipeInterface) mPagerAdapter.instantiateItem(mPager, i);
                if (fragment != null) {
                    fragment.onFragmentVisible();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    public ViewPager getViewPager() {
        return mPager;
    }


}
