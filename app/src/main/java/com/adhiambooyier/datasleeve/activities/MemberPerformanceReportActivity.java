package com.adhiambooyier.datasleeve.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.Survey;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class MemberPerformanceReportActivity extends AppCompatActivity {
    private AppCompatTextView txtSurveyTitle;
    private AppCompatTextView txtResponseCount;

    private FirebaseFirestore db;

    private String surveyId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_performance_report);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtSurveyTitle = findViewById(R.id.txtSurveyTitle);
        txtResponseCount = findViewById(R.id.txtResponseCount);

        surveyId = getIntent().getStringExtra("surveyId");
        db = FirebaseFirestore.getInstance();

        fetchSurvey();
    }
    public void fetchSurvey() {
        db.collection("surveys")
                .document(surveyId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Survey survey = task.getResult().toObject(Survey.class);
                            txtSurveyTitle.setText(survey.getTitle());
                            txtResponseCount.setText(String.valueOf(survey.getResponseCount()));

                        }
                    }
                });
    }

}
