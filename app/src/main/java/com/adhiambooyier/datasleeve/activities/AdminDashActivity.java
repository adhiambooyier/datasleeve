package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.adhiambooyier.datasleeve.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Locale;

public class AdminDashActivity extends AppCompatActivity {

    AppCompatTextView txtSignedInAs;
    CardView newManager;
    CardView surveys;
    CardView managers;
    CardView teams;
    CardView reports;
    CardView performance;

    FirebaseAuth mAuth;
    FirebaseFirestore db;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dash);

        txtSignedInAs = findViewById(R.id.txtSignedInAs);
        newManager = findViewById(R.id.newManager);
        surveys = findViewById(R.id.surveys);
        managers = findViewById(R.id.managers);
        teams = findViewById(R.id.teams);
        reports = findViewById(R.id.reports);
        performance = findViewById(R.id.performance);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = AdminDashActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        txtSignedInAs.setText(String.format(Locale.getDefault(), "Signed in as %s %s",
                sharedPreferences.getString("f_name", ""),
                sharedPreferences.getString("l_name", "")));

        View.OnClickListener cardOnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.newManager:
                        Intent intent = new Intent(AdminDashActivity.this, NewManagerActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.surveys:
                        intent = new Intent(AdminDashActivity.this, SurveysListActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.managers:
                        intent = new Intent(AdminDashActivity.this, ManagersActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.teams:
                        intent = new Intent(AdminDashActivity.this, TeamListActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.reports:
                        intent = new Intent(AdminDashActivity.this, SurveysListActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.performance:
                        intent = new Intent(AdminDashActivity.this, TeamReportActivity.class);
                        startActivity(intent);
                        return;

                    default:
                        return;
                }
            }
        };

        newManager.setOnClickListener(cardOnClick);
        surveys.setOnClickListener(cardOnClick);
        managers.setOnClickListener(cardOnClick);
        teams.setOnClickListener(cardOnClick);
        reports.setOnClickListener(cardOnClick);
        performance.setOnClickListener(cardOnClick);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.signout:
                mAuth.signOut();
                Intent intent = new Intent(AdminDashActivity.this, LoginActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(AdminDashActivity.this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
