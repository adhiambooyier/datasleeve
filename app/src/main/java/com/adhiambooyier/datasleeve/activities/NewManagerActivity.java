package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.NewUserRequest;
import com.adhiambooyier.datasleeve.models.User;
import com.adhiambooyier.datasleeve.utils.ApiClient;
import com.adhiambooyier.datasleeve.utils.ApiInterface;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewManagerActivity extends AppCompatActivity {
    AppCompatImageView logo;
    AppCompatTextView banner;
    AppCompatEditText txtFirstName;
    AppCompatEditText txtLastName;
    AppCompatEditText txtEmail;
    AppCompatEditText txtPassword;
    AppCompatEditText txtConfirmPassword;
    AppCompatButton btnCreateAcc;
    private TextInputLayout txtFirstNameWrap;
    private TextInputLayout txtLastnameWrap;
    private TextInputLayout txtEmailWrap;
    private TextInputLayout txtPasswordWrap;
    private TextInputLayout txtConfirmPasswordWrap;

    FirebaseAuth mAuth;
    FirebaseFirestore db;
    private SharedPreferences sharedPreferences;

    private final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_manager);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = NewManagerActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        logo = findViewById(R.id.logo);
        banner = findViewById(R.id.banner);
        txtFirstName = findViewById(R.id.txtFirstName);
        txtLastName = findViewById(R.id.txtLastName);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        txtConfirmPassword = findViewById(R.id.txtConfirmPassword);
        btnCreateAcc = findViewById(R.id.btnCreateAcc);
        txtFirstNameWrap = findViewById(R.id.txtFirstNameWrap);
        txtLastnameWrap = findViewById(R.id.txtLastNameWrap);
        txtEmailWrap = findViewById(R.id.txtEmailWrap);
        txtPasswordWrap = findViewById(R.id.txtPasswordWrap);
        txtConfirmPasswordWrap = findViewById(R.id.txtPasswordConfirmWrap);

        btnCreateAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fName = txtFirstName.getText().toString().trim();
                String lName = txtLastName.getText().toString().trim();
                String email = txtEmail.getText().toString().trim();
                String password = txtPassword.getText().toString();
                String confirmPassword = txtConfirmPassword.getText().toString();

                if (email.isEmpty()) {
                    txtEmailWrap.setError(null);
                    txtEmailWrap.setError("Email is required");
                } else {
                    txtEmailWrap.setErrorEnabled(false);
                }
                if (fName.isEmpty()) {
                    txtFirstNameWrap.setError(null);
                    txtFirstNameWrap.setError("First name is required");
                } else {
                    txtFirstNameWrap.setErrorEnabled(false);
                }
                if (lName.isEmpty()) {
                    txtLastnameWrap.setError(null);
                    txtLastnameWrap.setError("Last name is required");
                } else {
                    txtLastnameWrap.setErrorEnabled(false);
                }
                if (password.isEmpty()) {
                    txtPasswordWrap.setError(null);
                    txtPasswordWrap.setError("Password is required");
                } else {
                    txtPasswordWrap.setErrorEnabled(false);
                }
                if (confirmPassword.isEmpty() || !confirmPassword.equals(password)) {
                    txtConfirmPasswordWrap.setError(null);
                    txtConfirmPasswordWrap.setError("Passwords do not match");
                } else {
                    txtConfirmPasswordWrap.setErrorEnabled(false);
                }

                if (!email.isEmpty()
                        && !fName.isEmpty()
                        && !lName.isEmpty()
                        && !password.isEmpty()
                        && !confirmPassword.isEmpty()) {
                    User user = new User(fName, lName, email, "manager");
                    register(user, password);
                } else {
                    Toast.makeText(NewManagerActivity.this, "you are missing a field", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //check that the person creating account is logged in as admin
    //manager has organization
    private void register(final User user, final String password) {
        btnCreateAcc.setText("Creating account...");
        btnCreateAcc.setEnabled(false);
        ApiInterface apiInterface = ApiClient.getClient()
                .create(ApiInterface.class);
        Call<com.adhiambooyier.datasleeve.models.Response> call = apiInterface
                .createNewUser(new NewUserRequest(mAuth.getCurrentUser().getUid(),
                        sharedPreferences.getString("organization_id", null),
                        user.getfName(),
                        user.getlName(),
                        user.getEmail(),
                        password,
                        user.getUserType()));
        call.enqueue(new Callback<com.adhiambooyier.datasleeve.models.Response>() {
            @Override
            public void onResponse(@NonNull Call<com.adhiambooyier.datasleeve.models.Response> call,
                                   @NonNull Response<com.adhiambooyier.datasleeve.models.Response> response) {
                Log.d(TAG, "Response Code: " + response.code());
                Log.d(TAG, "Response Body: " + response.body());
                if (response.isSuccessful()) {
                    new AlertDialog.Builder(NewManagerActivity.this)
                            .setTitle("Server response")
                            .setMessage("User created sucessfully")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    startActivity(new Intent(NewManagerActivity.this, AdminDashActivity.class));
                                }
                            })
                            .show();
                } else {
                    Log.d(TAG, response.errorBody().toString());
                    errorDialog(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<com.adhiambooyier.datasleeve.models.Response> call,
                                  @NonNull Throwable t) {
                Log.d(TAG, "ERROR", t);
                errorDialog(t.getMessage());
            }
        });
    }

    private void errorDialog(String message) {
        new AlertDialog.Builder(NewManagerActivity.this)
                .setTitle("Server response")
                .setMessage("Failed to create user: " + message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        btnCreateAcc.setText("Create Account");
                        btnCreateAcc.setEnabled(true);
                    }
                })
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

