package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.adapters.ManagersAdapter;
import com.adhiambooyier.datasleeve.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class TeamListActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;
    private List<User> teams;
    private ManagersAdapter managersAdapter;

    private SwipeRefreshLayout teamsRefresh;
    private RecyclerView teamsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = TeamListActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);
        teams = new ArrayList<>();
        managersAdapter = new ManagersAdapter(teams);

        teamsRefresh = findViewById(R.id.teamsRefresh);
        teamsRecycler = findViewById(R.id.teamssRecycler);

        teamsRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        teamsRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchTeams();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TeamListActivity.this);
        teamsRecycler.setLayoutManager(linearLayoutManager);
        teamsRecycler.setItemAnimator(new DefaultItemAnimator());
        teamsRecycler.setAdapter(managersAdapter);

        fetchTeams();
    }

    private void fetchTeams() {
        teamsRefresh.setRefreshing(true);
        db.collection("users")
                .whereEqualTo("userType", "teamMember")
                .whereEqualTo("organizationId", sharedPreferences.getString("organization_id", null))
                .get()
                .addOnCompleteListener(TeamListActivity.this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            teams.clear();
                            managersAdapter.notifyDataSetChanged();

                            for (DocumentSnapshot document : task.getResult()) {
                                teams.add(document.toObject(User.class));
                                managersAdapter.notifyDataSetChanged();
                            }
                        }

                        teamsRefresh.setRefreshing(false);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.search:

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
