package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.adapters.SurveyListAdapter;
import com.adhiambooyier.datasleeve.models.Survey;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class SurveysListActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;

    private List<Survey> surveys;
    private SurveyListAdapter surveyListAdapter;

    private SwipeRefreshLayout surveysRefresh;
    private RecyclerView surveysRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surveys_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = SurveysListActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        surveys = new ArrayList<>();
        surveyListAdapter = new SurveyListAdapter(SurveysListActivity.this, surveys);

        surveysRefresh = findViewById(R.id.surveysRefresh);
        surveysRecycler = findViewById(R.id.surveysRecycler);

        surveysRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        surveysRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchSurveys();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SurveysListActivity.this);
        surveysRecycler.setLayoutManager(linearLayoutManager);
        surveysRecycler.setItemAnimator(new DefaultItemAnimator());
        surveysRecycler.setAdapter(surveyListAdapter);

        fetchSurveys();
    }

    private void fetchSurveys() {
        surveysRefresh.setRefreshing(true);
        String userType = sharedPreferences.getString("user_type", null);
        switch (userType) {
            case "admin":
                db.collection("surveys")
                        .whereEqualTo("organizationId", sharedPreferences.getString("organization_id", null))
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    surveys.clear();
                                    surveyListAdapter.notifyDataSetChanged();

                                    for (DocumentSnapshot document : task.getResult()) {
                                        Survey survey = document.toObject(Survey.class);
                                        survey.setId(document.getId());
                                        surveys.add(survey);
                                        surveyListAdapter.notifyDataSetChanged();
                                    }
                                    surveysRefresh.setRefreshing(false);
                                }
                            }
                        });
                break;
            case "manager":
                db.collection("surveys")
                        .whereEqualTo("createdById", mAuth.getCurrentUser().getUid())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    surveys.clear();
                                    surveyListAdapter.notifyDataSetChanged();

                                    for (DocumentSnapshot document : task.getResult()) {
                                        Survey survey = document.toObject(Survey.class);
                                        survey.setId(document.getId());
                                        surveys.add(survey);
                                        surveyListAdapter.notifyDataSetChanged();
                                    }
                                    surveysRefresh.setRefreshing(false);
                                }
                            }
                        });
                break;
            case "teamMember":
                db.collection("surveys")
                        .whereArrayContains("teamMembers", mAuth.getCurrentUser().getUid())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    surveys.clear();
                                    surveyListAdapter.notifyDataSetChanged();

                                    for (DocumentSnapshot document : task.getResult()) {
                                        Survey survey = document.toObject(Survey.class);
                                        survey.setId(document.getId());
                                        surveys.add(survey);
                                        surveyListAdapter.notifyDataSetChanged();
                                    }
                                    surveysRefresh.setRefreshing(false);
                                }
                            }
                        });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.search:

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
