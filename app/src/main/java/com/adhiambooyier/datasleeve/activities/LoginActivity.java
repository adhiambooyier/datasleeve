package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class LoginActivity extends AppCompatActivity {

    AppCompatEditText txtEmail;
    AppCompatEditText txtPassword;
    AppCompatButton btnSignIn;
    AppCompatButton btnCreateOrganisation;

    FirebaseAuth auth;
    FirebaseFirestore db;
    SharedPreferences sharedPreferences;
    final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.LaunchTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        btnCreateOrganisation = findViewById(R.id.btnCreateOrganization);

        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = LoginActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = txtEmail.getText().toString().trim();
                String password = txtPassword.getText().toString();
                if (!email.isEmpty() && !password.isEmpty()) {
                    login(email, password);
                } else {
                    Toast.makeText(LoginActivity.this, "You are missing email or passsword", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnCreateOrganisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseUser fbUser = auth.getCurrentUser();
        String userType = sharedPreferences.getString("user_type", null);
        if (fbUser != null && userType != null) {
            switch (userType) {
                case "admin":
                    startActivity(new Intent(LoginActivity.this, AdminDashActivity.class));
                    ActivityCompat.finishAffinity(LoginActivity.this);
                    break;

                case "manager":
                    startActivity(new Intent(LoginActivity.this, ManagerDashActivity.class));
                    ActivityCompat.finishAffinity(LoginActivity.this);
                    break;

                case "teamMember":
                    startActivity(new Intent(LoginActivity.this, TeamDashActivity.class));
                    ActivityCompat.finishAffinity(LoginActivity.this);
                    break;

                default:
                    auth.signOut();
                    sharedPreferences.edit().clear().apply();
                    break;
            }
        } else {
            auth.signOut();
            sharedPreferences.edit().clear().apply();
        }
    }

    private void login(final String email, final String password) {
        btnSignIn.setText("Signing you in");
        btnSignIn.setEnabled(false);
        btnCreateOrganisation.setEnabled(false);

        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            navigateToDashboard();
                        } else {
                            Log.w(TAG, "Signin failed", task.getException());
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            btnSignIn.setText("Sign In");
                            btnSignIn.setEnabled(true);
                            btnCreateOrganisation.setEnabled(true);
                        }
                    }
                });
    }

    private void navigateToDashboard() {
        db.collection("users")
                .document(auth.getCurrentUser().getUid())
                .get()
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            final User currentUser = task.getResult().toObject(User.class);

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("f_name", currentUser.getfName());
                            editor.putString("l_name", currentUser.getlName());
                            editor.putString("organization_id", currentUser.getOrganizationId());
                            editor.putString("user_type", currentUser.getUserType());
                            editor.apply();

                            switch (currentUser.getUserType()) {
                                case "admin":
                                    startActivity(new Intent(LoginActivity.this, AdminDashActivity.class));
                                    ActivityCompat.finishAffinity(LoginActivity.this);
                                    break;
                                case "manager":
                                    startActivity(new Intent(LoginActivity.this, ManagerDashActivity.class));
                                    ActivityCompat.finishAffinity(LoginActivity.this);
                                    break;
                                case "teamMember":
                                    startActivity(new Intent(LoginActivity.this, TeamDashActivity.class));
                                    ActivityCompat.finishAffinity(LoginActivity.this);
                                    break;
                                default:
                                    Log.w(TAG, "Signin failed", task.getException());
                                    Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.w(TAG, "Signin failed", task.getException());
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            btnSignIn.setText("Sign In");
                            btnSignIn.setEnabled(true);
                            btnCreateOrganisation.setEnabled(true);
                        }
                    }
                });
    }
}
