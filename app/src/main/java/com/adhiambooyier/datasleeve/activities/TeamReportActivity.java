package com.adhiambooyier.datasleeve.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class TeamReportActivity extends AppCompatActivity {
    private SwipeRefreshLayout teamReportRefresh;
    private ListView teamReportList;
    private List<String> teamData;
    private ArrayAdapter<String> teamDataAdapter;

    private FirebaseFirestore db;

    private final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_report);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        db = FirebaseFirestore.getInstance();

        teamReportRefresh = findViewById(R.id.teamReportRefresh);
        teamReportList = findViewById(R.id.teamReportList);

        teamReportRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent);
        teamReportRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchTeamMembers();
            }
        });

        teamData = new ArrayList<>();
        teamDataAdapter = new ArrayAdapter<>(TeamReportActivity.this, android.R.layout.simple_list_item_1, teamData);
        teamReportList.setAdapter(teamDataAdapter);

        fetchTeamMembers();
    }

    private void fetchTeamMembers() {
        teamReportRefresh.setRefreshing(true);
        db.collection("users")
                .whereEqualTo("userType", "teamMember")
                .get()
                .addOnCompleteListener(TeamReportActivity.this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, "team count :" + task.getResult().size());

                                final int nextIndex = teamData.size();
                                User user = document.toObject(User.class);
                                final String userName = user.getfName() + " " + user.getlName();
                                teamData.add(userName);
                                teamDataAdapter.notifyDataSetChanged();

                                db.collection("responses")
                                        .whereEqualTo("recordedBy", user.getId())
                                        .get()
                                        .addOnCompleteListener(TeamReportActivity.this, new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    teamData.set(nextIndex, userName + ": " + task.getResult().size());
                                                    teamDataAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        });
                            }
                            teamReportRefresh.setRefreshing(false);
                        }
                    }
                });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
