package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.adapters.QuestionsAdapter;
import com.adhiambooyier.datasleeve.models.Question;
import com.adhiambooyier.datasleeve.models.Survey;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class SurveyActivity extends AppCompatActivity {
    private AppCompatTextView txtSurveyTitle;
    private RecyclerView questionsRecycler;
    private SwipeRefreshLayout questionsRefresh;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;

    private QuestionsAdapter questionsAdapter;
    private List<Question> questionList;

    private final String TAG = getClass().getSimpleName();
    private String surveyId;
    private int responseCount = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        txtSurveyTitle = findViewById(R.id.surveyTitle);
        questionsRecycler = findViewById(R.id.questionsRecycler);
        questionsRefresh = findViewById(R.id.questionsRefresh);

        surveyId = getIntent().getStringExtra("surveyId");
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = SurveyActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        questionList = new ArrayList<>();
        questionsAdapter = new QuestionsAdapter(SurveyActivity.this, questionList);

        questionsRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        questionsRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchQuestions();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SurveyActivity.this);
        questionsRecycler.setLayoutManager(linearLayoutManager);
        questionsRecycler.setItemAnimator(new DefaultItemAnimator());
        questionsRecycler.setAdapter(questionsAdapter);

        fetchSurvey();

        fetchQuestions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_response, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.newResponse:
                fetchQuestions();
                ++responseCount;
                db.collection("surveys")
                        .document(surveyId)
                        .update("responseCount", responseCount)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(SurveyActivity.this, "Response count is: " + responseCount, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void fetchQuestions() {
        questionsRefresh.setRefreshing(true);
        db.collection("questions")
                .whereEqualTo("surveyId", surveyId)
                .orderBy("dateCreated")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            questionsAdapter = new QuestionsAdapter(SurveyActivity.this, questionList);
                            questionsRecycler.setAdapter(questionsAdapter);

                            int questionListSize = questionList.size();
                            questionList.clear();
                            questionsAdapter.notifyDataSetChanged();
                            for (DocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questionList.add(question);
                            }

                            questionsAdapter.notifyDataSetChanged();
                        }
                        questionsRefresh.setRefreshing(false);
                    }
                });
    }

    public void fetchSurvey() {
        db.collection("surveys")
                .document(surveyId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Survey survey = task.getResult().toObject(Survey.class);
                            txtSurveyTitle.setText(survey.getTitle());

                        }
                    }
                });
    }
}
