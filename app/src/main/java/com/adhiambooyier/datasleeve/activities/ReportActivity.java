package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.adapters.ResponseAdapter;
import com.adhiambooyier.datasleeve.models.Question;
import com.adhiambooyier.datasleeve.models.Survey;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReportActivity extends AppCompatActivity {
    private AppCompatTextView txtSurveyTitle;
    private AppCompatTextView txtResponseCount;
    private RecyclerView questionsRecycler;
    private SwipeRefreshLayout questionsRefresh;

    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;

    private ResponseAdapter responseAdapter;
    private List<Question> questionsList;

    private final String TAG = getClass().getSimpleName();
    private String surveyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtSurveyTitle = findViewById(R.id.txtSurveyTitle);
        txtResponseCount = findViewById(R.id.txtResponseCount);
        questionsRecycler = findViewById(R.id.questionsRecycler);
        questionsRefresh = findViewById(R.id.questionsRefresh);

        surveyId = getIntent().getStringExtra("surveyId");
        db = FirebaseFirestore.getInstance();
        sharedPreferences = getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);
        fetchSurvey();

        questionsList = new ArrayList<>();
        responseAdapter = new ResponseAdapter(ReportActivity.this, questionsList);

        questionsRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        questionsRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchQuestions();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReportActivity.this);
        questionsRecycler.setLayoutManager(linearLayoutManager);
        questionsRecycler.setItemAnimator(new DefaultItemAnimator());
        questionsRecycler.setAdapter(responseAdapter);

        fetchSurvey();
        fetchQuestions();
    }

    public void fetchSurvey() {
        db.collection("surveys")
                .document(surveyId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Survey survey = task.getResult().toObject(Survey.class);
                            txtSurveyTitle.setText(survey.getTitle());
                            txtResponseCount.setText(String.valueOf(survey.getResponseCount()));

                        }
                    }
                });
    }

    public void fetchQuestions() {
        questionsRefresh.setRefreshing(false);
        db.collection("questions")
                .whereEqualTo("surveyId", surveyId)
                .orderBy("dateCreated")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            questionsList = new ArrayList<>();
                            responseAdapter = new ResponseAdapter(ReportActivity.this, questionsList);
                            questionsRecycler.setAdapter(responseAdapter);
                            for (DocumentSnapshot document : task.getResult()) {
                                final Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questionsList.add(question);
                                responseAdapter.notifyDataSetChanged();
                                for (int i = 0; i < question.getChoices().size(); i++) {
                                    final int finalI = i;
                                    db.collection("responses")
                                            .whereEqualTo("questionId", question.getId())
                                            .whereArrayContains("answer", question.getChoices().get(i))
                                            .get()
                                            .addOnCompleteListener(ReportActivity.this, new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        question.getChoices().set(finalI, question.getChoices().get(finalI) + ": " + task.getResult().size());
                                                        responseAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            });
                                }
                                try {
                                    exportData(questionsList);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            questionsRefresh.setRefreshing(false);
                        }
                    }
                });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void exportData(List<Question> list) throws IOException {
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileName = "AnalysisData.csv";
        String filePath = baseDir + File.separator + fileName;
        File f = new File(filePath);
        CSVWriter writer;
        // File exist
        if (f.exists() && !f.isDirectory()) {
            FileWriter mFileWriter = new FileWriter(filePath, true);
            writer = new CSVWriter(mFileWriter);
        } else {
            writer = new CSVWriter(new FileWriter(filePath));
        }

        for (Question q : list) {
            for (String c : q.getChoices()) {
                String[] data = {q.getQuestion(), c};
                writer.writeNext(data);
            }
        }
        writer.close();

    }

}
