package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.adhiambooyier.datasleeve.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Locale;

public class ManagerDashActivity extends AppCompatActivity {

    AppCompatTextView txtSignedInAs;
    CardView newSurvey;
    CardView surveys;
    CardView newMember;
    CardView teams;
    CardView reports;
    CardView performance;

    FirebaseAuth mAuth;
    FirebaseFirestore db;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_dash);

        txtSignedInAs = findViewById(R.id.txtSignedInAs);
        newSurvey = findViewById(R.id.newSurvey);
        surveys = findViewById(R.id.surveys);
        newMember = findViewById(R.id.newMember);
        teams = findViewById(R.id.teams);
        reports = findViewById(R.id.reports);
        performance = findViewById(R.id.performance);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = ManagerDashActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        txtSignedInAs.setText(String.format(Locale.getDefault(), "Signed in as %s %s",
                sharedPreferences.getString("f_name", ""),
                sharedPreferences.getString("l_name", "")));

        View.OnClickListener cardOnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.newSurvey:
                        Intent intent = new Intent(ManagerDashActivity.this, NewSurveyActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.surveys:
                        intent = new Intent(ManagerDashActivity.this, ManagerSurveyListActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.newMember:
                        intent = new Intent(ManagerDashActivity.this, NewMemberActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.teams:
                        intent = new Intent(ManagerDashActivity.this, TeamListActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.reports:
                        intent = new Intent(ManagerDashActivity.this, SurveysListActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.performance:
                        intent = new Intent(ManagerDashActivity.this, TeamReportActivity.class);
                        startActivity(intent);
                        return;

                    default:
                        return;
                }
            }
        };

        newSurvey.setOnClickListener(cardOnClick);
        surveys.setOnClickListener(cardOnClick);
        newMember.setOnClickListener(cardOnClick);
        teams.setOnClickListener(cardOnClick);
        reports.setOnClickListener(cardOnClick);
        performance.setOnClickListener(cardOnClick);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.signout:
                mAuth.signOut();
                Intent intent = new Intent(ManagerDashActivity.this, LoginActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(ManagerDashActivity.this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
