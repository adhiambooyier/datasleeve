package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.adhiambooyier.datasleeve.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Locale;

public class TeamDashActivity extends AppCompatActivity {
    AppCompatTextView txtSignedInAs;
    CardView surveys;
    CardView results;

    FirebaseAuth mAuth;
    FirebaseFirestore db;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_dash);
        txtSignedInAs = findViewById(R.id.txtSignedInAs);
        surveys = findViewById(R.id.surveys);
        results = findViewById(R.id.results);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = TeamDashActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        txtSignedInAs.setText(String.format(Locale.getDefault(), "Signed in as %s %s",
                sharedPreferences.getString("f_name", ""),
                sharedPreferences.getString("l_name", "")));

        View.OnClickListener cardOnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.surveys:
                        Intent intent = new Intent(TeamDashActivity.this, SurveysListActivity.class);
                        startActivity(intent);
                        return;

                    case R.id.results:
                        intent = new Intent(TeamDashActivity.this, TeamSurveyListActivity.class);
                        startActivity(intent);
                        return;

                    default:
                        return;
                }
            }
        };

        surveys.setOnClickListener(cardOnClick);
        results.setOnClickListener(cardOnClick);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.signout:
                mAuth.signOut();
                Intent intent = new Intent(TeamDashActivity.this, LoginActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(TeamDashActivity.this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
