package com.adhiambooyier.datasleeve.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.adapters.ManagersAdapter;
import com.adhiambooyier.datasleeve.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ManagersActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;
    private List<User> managers;
    private ManagersAdapter managersAdapter;

    private SwipeRefreshLayout managersRefresh;
    private RecyclerView managersRecycler;;
    private AppCompatImageView iconEdit;
    private AppCompatImageView iconDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_managers);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        iconEdit = findViewById(R.id.iconEdit);
        iconDelete = findViewById(R.id.iconDelete);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = ManagersActivity.this.getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);
        managers = new ArrayList<>();
        managersAdapter = new ManagersAdapter(managers);

        managersRefresh = findViewById(R.id.managersRefresh);
        managersRecycler = findViewById(R.id.managersRecycler);

        managersRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        managersRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchManagers();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ManagersActivity.this);
        managersRecycler.setLayoutManager(linearLayoutManager);
        managersRecycler.setItemAnimator(new DefaultItemAnimator());
        managersRecycler.setAdapter(managersAdapter);
/*

        iconDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            */
/*    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                user.delete()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "User account deleted.");
                                }
                            }
                        });*//*

            }
        });
*/

     /*   iconEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            *//*    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                user.updateEmail("user@example.com")
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "User email address updated.");
                                }
                            }
                        });*//*
                *//*FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String newPassword = "SOME-SECURE-PASSWORD";

                user.updatePassword(newPassword)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "User password updated.");
                                }
                            }
                        });*//*
            }
        });*/
        fetchManagers();
    }

    private void fetchManagers() {
        managersRefresh.setRefreshing(true);
        db.collection("users")
                .whereEqualTo("userType", "manager")
                .whereEqualTo("organizationId", sharedPreferences.getString("organization_id", null))
                .get()
                .addOnCompleteListener(ManagersActivity.this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            managers.clear();
                            managersAdapter.notifyDataSetChanged();

                            for (DocumentSnapshot document : task.getResult()) {
                                managers.add(document.toObject(User.class));
                                managersAdapter.notifyDataSetChanged();
                            }
                        }

                        managersRefresh.setRefreshing(false);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.search:

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
