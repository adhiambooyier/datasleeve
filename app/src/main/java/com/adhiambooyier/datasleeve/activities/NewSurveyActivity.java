package com.adhiambooyier.datasleeve.activities;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.adapters.ViewPagerAdapter;
import com.adhiambooyier.datasleeve.fragments.SurveyPreviewFragment;
import com.adhiambooyier.datasleeve.fragments.SurveyQuestionFragment;
import com.adhiambooyier.datasleeve.fragments.SurveyTitleFragment;

import java.util.ArrayList;
import java.util.List;

public class NewSurveyActivity extends AppCompatActivity {

    private ViewPager mPager;
    private ViewPagerAdapter mPagerAdapter;
    private TabLayout surveyTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_survey);

        List<Fragment> fragments = new ArrayList<>();
        List<CharSequence> titles = new ArrayList<>();

        fragments.add(SurveyTitleFragment.newInstance());
        fragments.add(SurveyQuestionFragment.newInstance());
        fragments.add(SurveyPreviewFragment.newInstance());
        titles.add("New Survey");
        titles.add("Questions");
        titles.add("Preview");

        mPager = findViewById(R.id.surveyPager);
        surveyTabs = findViewById(R.id.surveyTabs);
        mPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments, titles);
        mPager.setAdapter(mPagerAdapter);
        surveyTabs.setupWithViewPager(mPager);
    }

    public ViewPager getViewPager() {
        return mPager;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
