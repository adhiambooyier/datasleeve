package com.adhiambooyier.datasleeve.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.Question;
import com.adhiambooyier.datasleeve.models.Responses;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionViewHolder> {
    private Context mContext;
    List<Question> questionList;

    public QuestionsAdapter(Context context, List<Question> list) {
        this.questionList = list;
        this.mContext = context;
    }

    public class QuestionViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView txtQuestionNumber;
        private AppCompatTextView txtQuestion;
        private LinearLayoutCompat choicesLayout;
        private AppCompatButton btnSubmitResponse;
        private CardView questionCard;

        private FirebaseAuth mAuth;
        private FirebaseFirestore db;
        private SharedPreferences sharedPreferences;

        private List<String> answers = new ArrayList<>();

        private final String TAG = getClass().getSimpleName();

        QuestionViewHolder(@NonNull View itemView) {
            super(itemView);
            txtQuestion = itemView.findViewById(R.id.txtQuestion);
            txtQuestionNumber = itemView.findViewById(R.id.txtQuestionNumber);
            choicesLayout = itemView.findViewById(R.id.choicesLayout);
            btnSubmitResponse = itemView.findViewById(R.id.btnSubmitResponse);
            questionCard = itemView.findViewById(R.id.questionCard);

            db = FirebaseFirestore.getInstance();
            mAuth = FirebaseAuth.getInstance();

        }

        private void saveResponse(Responses response, final Question question) {
            db.collection("responses")
                    .add(response)
                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "response saved successfully");
                                int questionIndex = questionList.indexOf(question);
                                questionList.remove(question);
                                notifyItemRemoved(questionIndex);
                            } else {
                                Log.w(TAG, "failed to save response", task.getException());
                                Toast.makeText(mContext, "survey not found", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

        void bindViews(int position) {
            final Question question = questionList.get(position);
            txtQuestionNumber.setText(String.format(Locale.ENGLISH, "%d. ", position + 1));
            txtQuestion.setText(question.getQuestion());

            int radioGroupId = ViewCompat.generateViewId();
            if (question.getQuestiontype() == Question.QUESTION_TYPE_SINGLE_CHOICE) {
                RadioGroup radioGroup = new RadioGroup(mContext);
                radioGroup.setId(radioGroupId);
                radioGroup.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT));
                choicesLayout.addView(radioGroup);
            }

            final int openChoiceEditTexId = ViewCompat.generateViewId();
            if (question.getQuestiontype() == Question.QUESTION_TYPE_OPEN_CHOICE) {
                AppCompatEditText editText = new AppCompatEditText(mContext);
                editText.setId(openChoiceEditTexId);
                editText.setHint("Type answer here");
                editText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                choicesLayout.addView(editText);
            }
            for (final String choice : question.getChoices()) {
                switch (question.getQuestiontype()) {
                    case Question.QUESTION_TYPE_SINGLE_CHOICE:
                        AppCompatRadioButton radioButton = new AppCompatRadioButton(mContext);
                        radioButton.setText(choice);
                        radioButton.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT));
                        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b)
                                    answers.add(choice);
                                else
                                    answers.remove(choice);
                            }
                        });
                        ((RadioGroup) (choicesLayout.findViewById(radioGroupId))).addView(radioButton);
                        break;

                    case Question.QUESTION_TYPE_MULTI_CHOICE:
                        AppCompatCheckBox checkBox = new AppCompatCheckBox(mContext);
                        checkBox.setText(choice);
                        checkBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b)
                                    answers.add(choice);
                                else
                                    answers.remove(choice);
                            }
                        });
                        choicesLayout.addView(checkBox);
                        break;
                }
            }

            btnSubmitResponse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (question.getQuestiontype() == Question.QUESTION_TYPE_OPEN_CHOICE) {
                        answers.clear();
                        answers.add(((AppCompatEditText) choicesLayout.findViewById(openChoiceEditTexId)).getText().toString());
                    }

                    String createdBy = mAuth.getCurrentUser().getUid();
                    Responses response = new Responses(question.getId(), answers, createdBy);
                    saveResponse(response, question);
                }
            });

        }
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.question_layout, viewGroup, false);
        return new QuestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder questionViewHolder, int i) {
        questionViewHolder.bindViews(i);
    }

    @Override
    public int getItemCount() {
        return this.questionList.size();
    }

}
