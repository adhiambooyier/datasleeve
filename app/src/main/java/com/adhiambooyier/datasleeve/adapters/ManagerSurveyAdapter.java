package com.adhiambooyier.datasleeve.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.Survey;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.List;

public class ManagerSurveyAdapter extends RecyclerView.Adapter<ManagerSurveyAdapter.ManagerSurveyAdapterViewHolder> {
    private Context mContext;
    private List<Survey> surveysList;
    private SharedPreferences sharedPreferences;

    private final String TAG = getClass().getSimpleName();

    public ManagerSurveyAdapter(Context context, List<Survey> list) {
        this.mContext = context;
        this.surveysList = list;
    }

    class ManagerSurveyAdapterViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView surveyName;
        private AppCompatTextView surveyDescription;
        private AppCompatTextView surveyManager;
        private AppCompatTextView surveyProgress;
        private AppCompatImageButton surveyOptions;

        FirebaseFirestore db;

        public ManagerSurveyAdapterViewHolder(View itemView) {
            super(itemView);
            this.surveyName = itemView.findViewById(R.id.surveyName);
            this.surveyDescription = itemView.findViewById(R.id.surveyDescription);
            this.surveyManager = itemView.findViewById(R.id.surveyManager);
            this.surveyProgress = itemView.findViewById(R.id.surveyProgress);
            this.surveyOptions = itemView.findViewById(R.id.surveyOptions);

            db = FirebaseFirestore.getInstance();
            sharedPreferences = mContext.getSharedPreferences(mContext.getString(R.string.preferences_file), Context.MODE_PRIVATE);
        }

        void bind(int position) {
            final Survey survey = surveysList.get(position);

            this.surveyName.setText(survey.getTitle());
            this.surveyDescription.setText(survey.getDescription());
            this.surveyManager.setText(survey.getCreatedBy());

            final Date currentDate = new Date();
            if (currentDate.before(survey.getStartDate())) {
                this.surveyProgress.setText(R.string.not_started);
                this.surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyNotStarted));
            } else if (survey.getEndDate() != null && currentDate.after(survey.getStartDate()) && currentDate.before(survey.getEndDate())) {
                this.surveyProgress.setText(R.string.in_progress);
                this.surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyInProgress));
            } else if (survey.getEndDate() != null && currentDate.after(survey.getEndDate()) || currentDate.equals(survey.getEndDate())) {
                this.surveyProgress.setText(R.string.completed);
                this.surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyCompleted));
            }

            surveyOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(mContext, v);
                    MenuInflater inflater = popupMenu.getMenuInflater();
                    inflater.inflate(R.menu.survey_options_menu, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_start_survey:
                                    db.collection("surveys")
                                            .document(survey.getId())
                                            .update("startDate", new Date())
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        survey.setStartDate(currentDate);
                                                        surveyProgress.setText(R.string.in_progress);
                                                        surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyInProgress));
                                                        notifyDataSetChanged();
                                                    }
                                                }
                                            });
                                    return true;

                                case R.id.action_end_survey:
                                    if (survey.getEndDate() == null) {
                                        db.collection("surveys")
                                                .document(survey.getId())
                                                .update("endDate", new Date())
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            survey.setEndDate(currentDate);
                                                            surveyProgress.setText(R.string.completed);
                                                            surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyCompleted));
                                                            notifyDataSetChanged();
                                                        }
                                                    }
                                                });
                                    }
                                    return true;

                                case R.id.action_delete_survey:
                                    new AlertDialog.Builder(mContext)
                                            .setTitle("Confirm")
                                            .setMessage("Delete " + survey.getTitle() + "?")
                                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    db.collection("surveys")
                                                            .document(survey.getId())
                                                            .delete()
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    surveysList.remove(survey);
                                                                    notifyDataSetChanged();
                                                                    Toast.makeText(mContext, "Survey deleted", Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                }
                                            })
                                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            })
                                            .show();
                                    return true;

                                default:
                                    return false;
                            }
                        }
                    });
                    popupMenu.show();
                }
            });
        }
    }

    @NonNull
    @Override
    public ManagerSurveyAdapter.ManagerSurveyAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.survey_list_layout, parent, false);
        return new ManagerSurveyAdapterViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull ManagerSurveyAdapter.ManagerSurveyAdapterViewHolder holder, int i) {
        holder.bind(i);
    }

    @Override
    public int getItemCount() {
        return this.surveysList.size();
    }
}

