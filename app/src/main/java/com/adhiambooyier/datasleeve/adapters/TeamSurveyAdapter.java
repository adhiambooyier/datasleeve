package com.adhiambooyier.datasleeve.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.activities.QuestionResponsesActivity;
import com.adhiambooyier.datasleeve.models.Survey;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.List;

public class TeamSurveyAdapter extends RecyclerView.Adapter<TeamSurveyAdapter.TeamSurveyAdapterViewHolder> {
    private Context mContext;
    private List<Survey> surveysList;

    private final String TAG = getClass().getSimpleName();

    public TeamSurveyAdapter(Context context, List<Survey> list) {
        this.mContext = context;
        this.surveysList = list;
    }

    class TeamSurveyAdapterViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView surveyName;
        private AppCompatTextView surveyDescription;
        private AppCompatTextView surveyManager;
        private AppCompatTextView surveyProgress;
        FirebaseFirestore db;

        TeamSurveyAdapterViewHolder(View itemView) {
            super(itemView);
            this.surveyName = itemView.findViewById(R.id.surveyName);
            this.surveyDescription = itemView.findViewById(R.id.surveyDescription);
            this.surveyManager = itemView.findViewById(R.id.surveyManager);
            this.surveyProgress = itemView.findViewById(R.id.surveyProgress);

            db = FirebaseFirestore.getInstance();
        }

        void bind(int position) {
            final Survey survey = surveysList.get(position);

            this.surveyName.setText(survey.getTitle());
            this.surveyDescription.setText(survey.getDescription());
            this.surveyManager.setText(survey.getCreatedBy());

            final Date currentDate = new Date();
            if (currentDate.before(survey.getStartDate())) {
                this.surveyProgress.setText(R.string.not_started);
                this.surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyNotStarted));
            } else if (survey.getEndDate() != null && currentDate.after(survey.getStartDate()) && currentDate.before(survey.getEndDate())) {
                this.surveyProgress.setText(R.string.in_progress);
                this.surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyInProgress));
            } else if (survey.getEndDate() != null && currentDate.after(survey.getEndDate()) || currentDate.equals(survey.getEndDate())) {
                this.surveyProgress.setText(R.string.completed);
                this.surveyProgress.setTextColor(mContext.getResources().getColor(R.color.colorSurveyCompleted));
            }

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, QuestionResponsesActivity.class);
                    i.putExtra("surveyId", survey.getId());
                    mContext.startActivity(i);
                    Toast.makeText(itemView.getContext(), survey.getTitle(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @NonNull
    @Override
    public TeamSurveyAdapter.TeamSurveyAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.survey_list_layout, parent, false);
        return new TeamSurveyAdapter.TeamSurveyAdapterViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamSurveyAdapter.TeamSurveyAdapterViewHolder holder, int i) {
        holder.bind(i);
    }

    @Override
    public int getItemCount() {
        return this.surveysList.size();
    }
}
