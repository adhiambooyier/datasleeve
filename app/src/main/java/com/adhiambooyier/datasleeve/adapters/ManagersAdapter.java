package com.adhiambooyier.datasleeve.adapters;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.User;

import java.util.List;

public class ManagersAdapter extends RecyclerView.Adapter<ManagersAdapter.ManagersAdapterViewHolder> {

    List<User> managerList;

    class ManagersAdapterViewHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView iconManager;
        private AppCompatImageView iconEdit;
        private AppCompatImageView iconDelete;
        private AppCompatTextView managerName;

        public ManagersAdapterViewHolder(View itemView) {
            super(itemView);

            iconManager = itemView.findViewById(R.id.userIcon);
            iconEdit = itemView.findViewById(R.id.iconEdit);
            iconDelete = itemView.findViewById(R.id.iconDelete);
            managerName = itemView.findViewById(R.id.managerName);

        }
    }

    public ManagersAdapter(List<User> list) {
        this.managerList = list;
    }

    @Override
    public ManagersAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.manager, parent, false);
        return new ManagersAdapterViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(ManagersAdapter.ManagersAdapterViewHolder holder, int position) {
        User user = managerList.get(position);

        holder.managerName.setText(user.getfName() + " " + user.getlName());
    }

    @Override
    public int getItemCount() {
        return this.managerList.size();
    }
}

