package com.adhiambooyier.datasleeve.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.Question;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.QuestionViewHolder> {
    private List<Question> questionsList;
    private Context mContext;

    public SurveyAdapter(Context context, List<Question> list) {
        this.mContext = context;
        this.questionsList = list;

        setHasStableIds(true);
    }

    public class QuestionViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView txtQuestionNumber;
        AppCompatTextView txtQuestion;
        LinearLayoutCompat choicesLayout;

        QuestionViewHolder(@NonNull View itemView) {
            super(itemView);
            txtQuestion = itemView.findViewById(R.id.txtQuestion);
            txtQuestionNumber = itemView.findViewById(R.id.txtQuestionNumber);
            choicesLayout = itemView.findViewById(R.id.choicesLayout);
        }

        void bindViews(int position) {
            Question question = questionsList.get(position);
            txtQuestionNumber.setText(String.format(Locale.ENGLISH, "%d. ", position + 1));
            txtQuestion.setText(question.getQuestion());

            int radioGroupId = ViewCompat.generateViewId();
            if (question.getQuestiontype() == Question.QUESTION_TYPE_SINGLE_CHOICE) {
                RadioGroup radioGroup = new RadioGroup(mContext);
                radioGroup.setId(radioGroupId);
                radioGroup.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT));
                choicesLayout.addView(radioGroup);
            }

            if(question.getQuestiontype() == Question.QUESTION_TYPE_OPEN_CHOICE){
                AppCompatEditText editText = new AppCompatEditText(mContext);
                editText.setHint("Type answer here");
                editText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                choicesLayout.addView(editText);
            }
            for (String choice : question.getChoices()) {
                switch (question.getQuestiontype()) {
                    case Question.QUESTION_TYPE_SINGLE_CHOICE:
                        AppCompatRadioButton radioButton = new AppCompatRadioButton(mContext);
                        radioButton.setText(choice);
                        radioButton.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT));
                        ((RadioGroup) (choicesLayout.findViewById(radioGroupId))).addView(radioButton);
                        break;

                    case Question.QUESTION_TYPE_MULTI_CHOICE:
                        AppCompatCheckBox checkBox = new AppCompatCheckBox(mContext);
                        checkBox.setText(choice);
                        checkBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        choicesLayout.addView(checkBox);
                        break;
                }
            }
        }

    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.survey_layout, viewGroup, false);

        return new QuestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int i) {
        holder.bindViews(i);
    }

    @Override
    public int getItemCount() {
        return this.questionsList.size();
    }

    @Override
    public long getItemId(int position) {
        return ViewCompat.generateViewId();
    }
}
