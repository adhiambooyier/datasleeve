package com.adhiambooyier.datasleeve.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.models.Question;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;
import java.util.Locale;

public class ResponseAdapter extends RecyclerView.Adapter<ResponseAdapter.ResponseViewHolder> {
    private Context mContext;
    private List<Question> questionsList;

    public ResponseAdapter(Context context, List<Question> list) {
        this.questionsList = list;
        this.mContext = context;
    }

    public class ResponseViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView txtQuestionNumber;
        private AppCompatTextView txtQuestion;
        private AppCompatTextView answers;

        private final String TAG = getClass().getSimpleName();

        ResponseViewHolder(@NonNull View itemView) {
            super(itemView);
            txtQuestion = itemView.findViewById(R.id.txtQuestion);
            txtQuestionNumber = itemView.findViewById(R.id.txtQuestionNumber);
            answers = itemView.findViewById(R.id.answers);
        }

        void bindViews(int position) {
            answers.setText("");
            final Question question = questionsList.get(position);
            txtQuestionNumber.setText(String.valueOf(position + 1));
            txtQuestion.setText(question.getQuestion());
            for (String choice : question.getChoices())
                answers.setText(answers.getText().toString() + "\n" + choice);
        }

    }

    @NonNull
    @Override
    public ResponseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.question_response_layout, viewGroup, false);
        return new ResponseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ResponseViewHolder responseViewHolder, int i) {
        responseViewHolder.bindViews(i);
    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }
}
