package com.adhiambooyier.datasleeve.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.activities.NewSurveyActivity;
import com.adhiambooyier.datasleeve.models.Question;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SurveyQuestionFragment extends Fragment {
    private AppCompatTextView txtQuestionNumber;
    private TextInputLayout txtQuestionWrap;
    private AppCompatEditText txtQuestion;
    private AppCompatTextView txtQuestionType;
    private RadioGroup questionTypeRadioGroup;
    private AppCompatRadioButton radioChoice;
    private AppCompatRadioButton checkChoice;
    private AppCompatRadioButton openChoice;
    private AppCompatTextView txtAddChoices;
    private AppCompatButton btnAddChoice;
    private AppCompatButton btnSaveQuestion;
    private AppCompatButton btnAddQuestion;
    private AppCompatButton btnFinish;
    private LinearLayoutCompat choicesLayout;

    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;

    private ViewPager mPager;

    private final String TAG = getClass().getSimpleName();
    private int questionType = Question.QUESTION_TYPE_SINGLE_CHOICE;

    private List<AppCompatEditText> choiceEditTexts;
    private int choicesCount;
    private int questionsCount = 0;
    private String surveyId;


    public SurveyQuestionFragment() {
        // Required empty public constructor
    }

    public static SurveyQuestionFragment newInstance() {
        SurveyQuestionFragment fragment = new SurveyQuestionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_question, container, false);

        txtQuestionNumber = view.findViewById(R.id.txtQuestionNumber);
        txtQuestionWrap = view.findViewById(R.id.txtQuestionWrap);
        txtQuestion = view.findViewById(R.id.question);
        txtQuestionType = view.findViewById(R.id.txtQuestionType);
        questionTypeRadioGroup = view.findViewById(R.id.questionType);
        radioChoice = view.findViewById(R.id.radioChoice);
        checkChoice = view.findViewById(R.id.checkChoice);
        openChoice = view.findViewById(R.id.openChoice);
        txtAddChoices = view.findViewById(R.id.txtAddChoices);
        btnAddChoice = view.findViewById(R.id.btnAddChoice);
        btnSaveQuestion = view.findViewById(R.id.btnSaveQuestion);
        btnAddQuestion = view.findViewById(R.id.btnAddQuestion);
        btnFinish = view.findViewById(R.id.btnfinish);
        choicesLayout = view.findViewById(R.id.answers);

        choiceEditTexts = new ArrayList<>();

        mPager = ((NewSurveyActivity) getActivity()).getViewPager();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        txtQuestionNumber.setText(String.format(Locale.ENGLISH, "Question %d", ++questionsCount));

        questionTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.radioChoice:
                        questionType = Question.QUESTION_TYPE_SINGLE_CHOICE;
                        break;
                    case R.id.checkChoice:
                        questionType = Question.QUESTION_TYPE_MULTI_CHOICE;
                        break;
                    case R.id.openChoice:
                        questionType = Question.QUESTION_TYPE_OPEN_CHOICE;
                        btnAddChoice.setVisibility(View.GONE);
                        txtAddChoices.setVisibility(View.GONE);
                        break;
                }
            }
        });

        btnAddChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtQuestionWrap.setError(null);
                txtQuestionWrap.setErrorEnabled(false);
                AppCompatEditText editText = new AppCompatEditText(getContext());
                editText.setHint(String.format(Locale.ENGLISH, "Choice %d", ++choicesCount));
                editText.requestFocus();
                editText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                choicesLayout.addView(editText);
                choiceEditTexts.add(editText);
            }
        });

        btnSaveQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                surveyId = sharedPreferences.getString("surveyId", null);
                String question = txtQuestion.getText().toString().trim();
                List<String> choiceArray = new ArrayList<>();
                for (AppCompatEditText editText : choiceEditTexts) {
                    String choice = editText.getText().toString().trim();
                    if (!choice.isEmpty()) {
                        choiceArray.add(choice);
                    }
                }
                if (surveyId != null) {
                    if (!question.isEmpty()) {
                        if (choiceArray.size() != 0 || questionType == Question.QUESTION_TYPE_OPEN_CHOICE) {
                            Question question1 = new Question(surveyId, question, questionType, choiceArray);
                            saveQuestion(question1);
                        } else {
                            Log.d(TAG, "failed to save question");
                            txtQuestionWrap.setError("Question must have choices");
                            Toast.makeText(getContext(), "failed to save question", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        txtQuestionWrap.setError("Question is empty");
                        Toast.makeText(getContext(), "failed to save question", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "empty question");
                    }
                } else {
                    txtQuestionWrap.setError("Survey not found");
                    Toast.makeText(getContext(), "failed to save question", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "empty survey");
                }
            }
        });
        btnAddQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (surveyId != null) {
                    txtQuestionNumber.setText(String.format(Locale.ENGLISH, "Question %d", ++questionsCount));
                    txtQuestion.setText("");
                    choicesLayout.removeAllViews();
                    choiceEditTexts.clear();
                    choicesCount = 0;
                    openChoice.setChecked(false);
                    radioChoice.setChecked(false);
                    checkChoice.setChecked(false);
                    txtQuestionWrap.setError(null);
                    txtQuestionWrap.setErrorEnabled(false);
                    btnSaveQuestion.setText("save question");
                    btnSaveQuestion.setEnabled(true);
                    btnAddChoice.setVisibility(View.VISIBLE);
                } else {
                    txtQuestionWrap.setError("Survey not found");
                    Toast.makeText(getContext(), "survey not found", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "empty survey");
                }
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (surveyId != null) {
                    btnSaveQuestion.setVisibility(View.GONE);
                    btnAddChoice.setVisibility(View.GONE);
                    btnAddQuestion.setVisibility(View.GONE);
                    txtAddChoices.setVisibility(View.GONE);
                    questionTypeRadioGroup.setFocusable(false);
                    btnFinish.setText("Survey Saved!");
                    txtQuestionNumber.setText("");
                    txtQuestion.setText("");
                    choicesLayout.removeAllViews();
                    btnFinish.setEnabled(false);

                    mPager.setCurrentItem(2);
                } else {
                    txtQuestionWrap.setError("Survey not found");
                    Toast.makeText(getContext(), "survey not found", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "empty survey");
                }
            }
        });

        return view;
    }

    private void saveQuestion(Question question) {
        db.collection("questions")
                .add(question)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "question saved successfully");
                            btnSaveQuestion.setText("question saved");
                            btnSaveQuestion.setEnabled(false);
                            btnAddChoice.setVisibility(View.GONE);
                        } else {
                            Log.w(TAG, "failed to save question", task.getException());
                        }
                    }
                });
    }


}
