package com.adhiambooyier.datasleeve.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.activities.AdminDashActivity;
import com.adhiambooyier.datasleeve.models.Organization;
import com.adhiambooyier.datasleeve.models.User;
import com.adhiambooyier.datasleeve.utils.ViewPagerSwipeInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class NewOrganizationFragment extends Fragment implements ViewPagerSwipeInterface {

    AppCompatImageView logo;
    AppCompatTextView banner;
    AppCompatEditText txtOrgName;
    AppCompatButton btnCreateOrganization;
    AppCompatTextView txtOr;
    AppCompatButton btnSignIn;

    FirebaseAuth auth;
    FirebaseFirestore db;
    FirebaseUser fbUser;
    SharedPreferences sharedPreferences;
    final String TAG = getClass().getSimpleName();

    public NewOrganizationFragment() {
        // Required empty public constructor
    }

    public static NewOrganizationFragment newInstance() {
        NewOrganizationFragment fragment = new NewOrganizationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        fbUser = auth.getCurrentUser();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_organization, container, false);

        logo = view.findViewById(R.id.logo);
        banner = view.findViewById(R.id.banner);
        txtOrgName = view.findViewById(R.id.txtOrgName);
        btnCreateOrganization = view.findViewById(R.id.btnCreateOrganization);
        txtOr = view.findViewById(R.id.txtOr);
        btnSignIn = view.findViewById(R.id.btnSignIn);

        btnCreateOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtOrgName.getText().toString().trim();
                if (fbUser != null) {
                    createOrganization(name);
                }
            }
        });

        hideViews();

        return view;
    }

    @Override
    public void onFragmentVisible() {
        hideViews();
    }

    private void hideViews() {
        fbUser = auth.getCurrentUser();
        if (fbUser == null) {
            logo.setImageResource(R.drawable.ic_report_yellow_40dp);
            banner.setText("Create account first");
            txtOrgName.setVisibility(View.GONE);
            btnCreateOrganization.setVisibility(View.GONE);
            txtOr.setVisibility(View.GONE);
            btnSignIn.setVisibility(View.GONE);
        } else {
            logo.setImageResource(R.drawable.ic_pie_chart);
            banner.setText(getActivity().getString(R.string.create_your_organization));
            txtOrgName.setVisibility(View.VISIBLE);
            btnCreateOrganization.setVisibility(View.VISIBLE);
            txtOr.setVisibility(View.VISIBLE);
            btnSignIn.setVisibility(View.VISIBLE);
        }
    }

    private void createOrganization(String name) {
        final Organization organization = new Organization(name);
        db.collection("organizations")
                .add(organization)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "organization added successfully");
                            organization.setId(task.getResult().getId());
                            db.collection("organizations")
                                    .document(organization.getId())
                                    .update("id", organization.getId())
                                    .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                db.collection("users")
                                                        .document(fbUser.getUid())
                                                        .update("organizationId", organization.getId())
                                                        .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    db.collection("users")
                                                                            .document(fbUser.getUid())
                                                                            .get()
                                                                            .addOnCompleteListener(getActivity(), new OnCompleteListener<DocumentSnapshot>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                                                    if (task.isSuccessful()) {
                                                                                        User currentUser = task.getResult().toObject(User.class);

                                                                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                                                                        editor.putString("f_name", currentUser.getfName());
                                                                                        editor.putString("l_name", currentUser.getlName());
                                                                                        editor.putString("organization_id", currentUser.getOrganizationId());
                                                                                        editor.putString("user_type", currentUser.getUserType());
                                                                                        editor.apply();

                                                                                        Intent i = new Intent(getActivity(), AdminDashActivity.class);
                                                                                        startActivity(i);
                                                                                        ActivityCompat.finishAffinity(getActivity());
                                                                                    } else {
                                                                                        db.collection("organizations")
                                                                                                .document(organization.getId())
                                                                                                .delete();
                                                                                        Log.w(TAG, "save failed", task.getException());
                                                                                        Toast.makeText(getActivity(), "Error creating organization", Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                }
                                                                            });
                                                                } else {
                                                                    db.collection("organizations")
                                                                            .document(organization.getId())
                                                                            .delete();
                                                                    Log.w(TAG, "save failed", task.getException());
                                                                    Toast.makeText(getActivity(), "Error creating organization", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });
                                            } else {
                                                db.collection("organizations")
                                                        .document(organization.getId())
                                                        .delete();
                                                Log.w(TAG, "Update failed", task.getException());
                                                Toast.makeText(getActivity(), "Error creating organization", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        } else {
                            Log.w(TAG, "Save failed", task.getException());
                            Toast.makeText(getActivity(), "Error creating organization", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
}
