package com.adhiambooyier.datasleeve.fragments;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.activities.NewSurveyActivity;
import com.adhiambooyier.datasleeve.models.Survey;
import com.adhiambooyier.datasleeve.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SurveyTitleFragment extends Fragment {
    private AppCompatEditText txtStartDate;
    private AppCompatEditText txtSurveyTitle;
    private AppCompatEditText txtSurveyDescription;
    private AppCompatButton btnAddTeam;
    private AppCompatButton btnCreateSurvey;
    private TextInputLayout dateWrap;
    private List<String> team;
    private List<User> teamMembers;

    private final Calendar myCalendar = Calendar.getInstance();

    private SimpleDateFormat sdf;
    private final String TAG = getClass().getSimpleName();
    private ViewPager mPager;

    FirebaseAuth mAuth;
    FirebaseFirestore db;
    SharedPreferences sharedPreferences;

    CharSequence[] teamMembersString;
    boolean[] checkedItems;

    public SurveyTitleFragment() {
        // Required empty public constructor
    }

    public static SurveyTitleFragment newInstance() {
        SurveyTitleFragment fragment = new SurveyTitleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_title, container, false);
        txtStartDate = view.findViewById(R.id.txtStartDate);
        txtSurveyTitle = view.findViewById(R.id.txtSurveyTitle);
        txtSurveyDescription = view.findViewById(R.id.txtSurveyDescription);
        btnAddTeam = view.findViewById(R.id.btnAddTeam);
        btnCreateSurvey = view.findViewById(R.id.btnCreateSurvey);
        dateWrap = view.findViewById(R.id.dateWrap);

        mPager = ((NewSurveyActivity) getActivity()).getViewPager();
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        team = new ArrayList<>();
        teamMembers = new ArrayList<>();

        sdf = new SimpleDateFormat("dd/MM/yy", Locale.US);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                if (myCalendar.getTime().after(new Date())) {
                    updateLabel();
                } else {
                    Toast.makeText(getContext(), "Date is past!", Toast.LENGTH_SHORT).show();
                    new DatePickerDialog(getActivity(), this, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }

        };

        txtStartDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnAddTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Select your team");
                builder.setMultiChoiceItems(teamMembersString, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        checkedItems[which] = isChecked;
                        if (isChecked) {
                            team.add(teamMembers.get(which).getId());
                        } else {
                            team.remove(teamMembers.get(which).getId());
                        }
                    }
                });

                builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        btnCreateSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtSurveyTitle.getText().toString().trim();
                String description = txtSurveyDescription.getText().toString().trim();
                String startDate = txtStartDate.getText().toString().trim();
                String createdBy = sharedPreferences.getString("f_name", "") + " " + sharedPreferences.getString("l_name", "");
                String createdById = mAuth.getCurrentUser().getUid();
                String organizationId = sharedPreferences.getString("organization_id", null);


                if (!name.isEmpty() && !description.isEmpty() && !startDate.isEmpty() && organizationId != null) {
                    try {
                        Date dStartDate = sdf.parse(startDate);
                        Survey survey = new Survey(name, description, dStartDate, createdBy, createdById, organizationId, team);
                        createSurvey(survey);
                    } catch (ParseException e) {
                        Log.w(TAG, e);
                        dateWrap.setError("Date format is wrong");
                    }

                }

            }
        });

        fetchTeamMembers();

        return view;
    }

    private void fetchTeamMembers() {
        Log.d(TAG, "fetchTeamMembers method");
        db.collection("users")
                .whereEqualTo("userType", "teamMember")
                .whereEqualTo("organizationId", sharedPreferences.getString("organization_id", null))
                .get()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "fetchTeamMembers successful");
                            for (DocumentSnapshot document : task.getResult()) {
                                User teamMember = document.toObject(User.class);
                                teamMember.setId(document.getId());
                                teamMembers.add(teamMember);
                            }

                            teamMembersString = new CharSequence[teamMembers.size()];
                            checkedItems = new boolean[teamMembers.size()];
                            for (int i = 0; i < teamMembers.size(); i++) {
                                teamMembersString[i] = teamMembers.get(i).toString();
                                checkedItems[i] = false;
                            }
                        } else {
                            Log.w(TAG, task.getException());
                        }
                    }
                });
    }

    private void updateLabel() {
        txtStartDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void createSurvey(Survey survey) {
        btnCreateSurvey.setText("Creating survey...");
        btnCreateSurvey.setEnabled(false);
        db.collection("surveys")
                .add(survey)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Survey successfully created!");
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("surveyId", task.getResult().getId()).apply();
                            btnCreateSurvey.setText("Saved");
                            btnCreateSurvey.setEnabled(false);
                            mPager.setCurrentItem(1);
                        } else {
                            Log.w(TAG, "Error creating survey", task.getException());
                            btnCreateSurvey.setText("Create survey...");
                            btnCreateSurvey.setEnabled(true);
                        }
                    }
                });
    }

}
