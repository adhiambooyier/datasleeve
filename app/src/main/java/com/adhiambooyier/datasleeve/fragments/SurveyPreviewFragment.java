package com.adhiambooyier.datasleeve.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.activities.ManagerDashActivity;
import com.adhiambooyier.datasleeve.activities.SurveysListActivity;
import com.adhiambooyier.datasleeve.adapters.SurveyAdapter;
import com.adhiambooyier.datasleeve.adapters.SurveyListAdapter;
import com.adhiambooyier.datasleeve.models.Question;
import com.adhiambooyier.datasleeve.models.Survey;
import com.adhiambooyier.datasleeve.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class SurveyPreviewFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;

    private List<Question> questionsList;
    private SurveyAdapter surveyAdapter;

    private SwipeRefreshLayout surveyRefresh;
    private RecyclerView questionsRecycler;
    private AppCompatButton btnFinish;

    /*private AppCompatTextView surveyTitle;*/

    public SurveyPreviewFragment() {
        // Required empty public constructor
    }

    public static SurveyPreviewFragment newInstance() {
        SurveyPreviewFragment fragment = new SurveyPreviewFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_preview, container, false);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE);

        questionsList = new ArrayList<>();
        surveyAdapter = new SurveyAdapter(getActivity(), questionsList);

        surveyRefresh = view.findViewById(R.id.surveyRefresh);
        questionsRecycler = view.findViewById(R.id.questionsRecycler);
        btnFinish = view.findViewById(R.id.btnfinish);
        /*surveyTitle = view.findViewById(R.id.txtSurveyTitle);*/

        /*surveyTitle.setText(sharedPreferences.getString("surveyTitle", null));*/

        surveyRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        surveyRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                questionsList.clear();
                fetchQuestions();
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ManagerDashActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(getActivity());

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        questionsRecycler.setLayoutManager(linearLayoutManager);
        questionsRecycler.setItemAnimator(new DefaultItemAnimator());
        questionsRecycler.setAdapter(surveyAdapter);


        fetchQuestions();

        return view;
    }

    public void fetchQuestions() {
        surveyRefresh.setRefreshing(true);
        db.collection("questions")
                .whereEqualTo("surveyId", sharedPreferences.getString("surveyId", null))
                .orderBy("dateCreated")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            surveyAdapter = new SurveyAdapter(getContext(), questionsList);
                            questionsRecycler.setAdapter(surveyAdapter);
                            questionsList.clear();
                            surveyAdapter.notifyDataSetChanged();

                            for (DocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questionsList.add(question);
                                questionsList.add(question);
                                surveyAdapter.notifyDataSetChanged();
                            }
                        }
                        surveyRefresh.setRefreshing(false);
                    }
                });

    }

}
