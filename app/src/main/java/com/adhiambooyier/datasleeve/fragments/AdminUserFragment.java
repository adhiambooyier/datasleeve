package com.adhiambooyier.datasleeve.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adhiambooyier.datasleeve.R;
import com.adhiambooyier.datasleeve.activities.RegisterActivity;
import com.adhiambooyier.datasleeve.models.User;
import com.adhiambooyier.datasleeve.utils.ViewPagerSwipeInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class AdminUserFragment extends Fragment implements ViewPagerSwipeInterface {
    private AppCompatImageView logo;
    private AppCompatTextView banner;
    private AppCompatEditText txtFirstName;
    private AppCompatEditText txtLastName;
    private AppCompatEditText txtEmail;
    private AppCompatEditText txtPassword;
    private AppCompatEditText txtConfirmPassword;
    private AppCompatButton btnCreateAdmin;
    private AppCompatTextView txtOr;
    private TextInputLayout txtFirstNameWrap;
    private TextInputLayout txtLastnameWrap;
    private TextInputLayout txtEmailWrap;
    private TextInputLayout txtPasswordWrap;
    private TextInputLayout txtConfirmPasswordWrap;
    private AppCompatButton btnSignIn;

    FirebaseAuth auth;
    FirebaseFirestore db;

    ViewPager mPager;

    private final String TAG = getClass().getSimpleName();

    public AdminUserFragment() {
        // Required empty public constructor
    }

    public static AdminUserFragment newInstance() {
        AdminUserFragment fragment = new AdminUserFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admin_user, container, false);

        logo = view.findViewById(R.id.logo);
        banner = view.findViewById(R.id.banner);
        txtFirstName = view.findViewById(R.id.txtFirstName);
        txtLastName = view.findViewById(R.id.txtLastName);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtPassword = view.findViewById(R.id.txtPassword);
        txtConfirmPassword = view.findViewById(R.id.txtConfirmPassword);
        btnCreateAdmin = view.findViewById(R.id.btnCreateAdmin);
        txtOr = view.findViewById(R.id.txtOr);
        btnSignIn = view.findViewById(R.id.btnSignIn);
        txtFirstNameWrap = view.findViewById(R.id.txtFirstNameWrap);
        txtLastnameWrap = view.findViewById(R.id.txtLastNameWrap);
        txtEmailWrap = view.findViewById(R.id.txtEmailWrap);
        txtPasswordWrap = view.findViewById(R.id.txtPasswordWrap);
        txtConfirmPasswordWrap = view.findViewById(R.id.txtPasswordConfirmWrap);

        btnCreateAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fName = txtFirstName.getText().toString().trim();
                String lName = txtLastName.getText().toString().trim();
                String email = txtEmail.getText().toString().trim();
                String password = txtPassword.getText().toString();
                String confirmPassword = txtConfirmPassword.getText().toString();

                if (email.isEmpty()) {
                    txtEmailWrap.setError(null);
                    txtEmailWrap.setError("Email is required");
                } else {
                    txtEmailWrap.setErrorEnabled(false);
                }
                if (fName.isEmpty()) {
                    txtFirstNameWrap.setError(null);
                    txtFirstNameWrap.setError("First name is required");
                } else {
                    txtFirstNameWrap.setErrorEnabled(false);
                }
                if (lName.isEmpty()) {
                    txtLastnameWrap.setError(null);
                    txtLastnameWrap.setError("Last name is required");
                } else {
                    txtLastnameWrap.setErrorEnabled(false);
                }
                if (password.isEmpty()) {
                    txtPasswordWrap.setError(null);
                    txtPasswordWrap.setError("Password is required");
                } else {
                    txtPasswordWrap.setErrorEnabled(false);
                }
                if (confirmPassword.isEmpty() || !confirmPassword.equals(password)) {
                    txtConfirmPasswordWrap.setError(null);
                    txtConfirmPasswordWrap.setError("Passwords do not match");
                } else {
                    txtConfirmPasswordWrap.setErrorEnabled(false);
                }
                if (!email.isEmpty()
                        && !fName.isEmpty()
                        && !lName.isEmpty()
                        && !password.isEmpty()
                        && !confirmPassword.isEmpty()) {
                    User user = new User(fName, lName, email, "admin");
                    register(user, password);
                } else {
                    Toast.makeText(getActivity(), "you are missing a field", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mPager = ((RegisterActivity) getActivity()).getViewPager();
        hideViews();

        return view;
    }

    private void register(final User user, String password) {
        btnCreateAdmin.setText("Creating account...");
        btnCreateAdmin.setEnabled(false);
        auth.createUserWithEmailAndPassword(user.getEmail(), password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Registration successful");
                            final FirebaseUser fbUser = auth.getCurrentUser();
                            user.setId(fbUser.getUid());
                            db.collection("users")
                                    .document(user.getId())
                                    .set(user)
                                    .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "Save successful");
                                                mPager.setCurrentItem(1);
                                                btnCreateAdmin.setText("user created successfully!");
                                            } else {
                                                Log.w(TAG, task.getException().getMessage(), task.getException());
                                                Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                fbUser.delete();
                                                btnCreateAdmin.setText("Create Account");
                                                btnCreateAdmin.setEnabled(true);
                                            }
                                        }
                                    });
                        } else {
                            Log.w(TAG, "Registration failed", task.getException());
                            Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            btnCreateAdmin.setText("Create Account");
                            btnCreateAdmin.setEnabled(true);
                        }
                    }
                });
    }

    @Override
    public void onFragmentVisible() {
        hideViews();
    }

    private void hideViews() {
        FirebaseUser fbUser = auth.getCurrentUser();
        if (fbUser != null) {
            logo.setImageResource(R.drawable.ic_done_green_40dp);
            banner.setText("Account created successfully!");
            txtFirstName.setVisibility(View.GONE);
            txtLastName.setVisibility(View.GONE);
            txtEmail.setVisibility(View.GONE);
            txtPassword.setVisibility(View.GONE);
            txtConfirmPassword.setVisibility(View.GONE);
            btnCreateAdmin.setVisibility(View.GONE);
            txtOr.setVisibility(View.GONE);
            btnSignIn.setVisibility(View.GONE);
        }
    }
}
