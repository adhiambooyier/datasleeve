package com.adhiambooyier.datasleeve.models;

import java.util.Date;
import java.util.List;

public class Question {
    private String id;
    private String surveyId;
    private String question;
    private int questiontype;
    private Date dateCreated;
    private List<String> choices;

    public static final int QUESTION_TYPE_SINGLE_CHOICE = 1;
    public static final int QUESTION_TYPE_MULTI_CHOICE = 2;
    public static final int QUESTION_TYPE_OPEN_CHOICE = 3;

    public Question() {
    }

    public Question(String id, String surveyId, String question, int questiontype, List<String> choices) {
        this.id = id;
        this.surveyId = surveyId;
        this.question = question;
        this.questiontype = questiontype;
        this.dateCreated = new Date();
        this.choices = choices;
    }

    public Question(String surveyId, String question, int questiontype, List<String> choices) {
        this.surveyId = surveyId;
        this.question = question;
        this.questiontype = questiontype;
        this.dateCreated = new Date();
        this.choices = choices;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    public int getQuestiontype() {
        return questiontype;
    }

    public void setQuestiontype(int questiontype) {
        this.questiontype = questiontype;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
