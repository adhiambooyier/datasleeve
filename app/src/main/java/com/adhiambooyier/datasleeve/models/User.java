package com.adhiambooyier.datasleeve.models;

import java.util.Locale;

public class User {
    private String id;
    private String organizationId;
    private String fName;
    private String lName;
    private String email;
    private String userType;

    public User() {
    }

    public User(String id, String organizationId, String fName, String lName, String email, String userType) {
        this.id = id;
        this.organizationId = organizationId;
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.userType = userType;
    }

    public User(String fName, String lName, String email, String userType) {
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.userType = userType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "%s %s", this.fName, this.lName);
    }
}
