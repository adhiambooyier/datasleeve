package com.adhiambooyier.datasleeve.models;

import java.util.Date;
import java.util.List;

public class Responses {
    private String id;
    private String questionId;
    private List<String> answer;
    private String recordedBy;
    private Date timestamp;


    public Responses(){

    }
    public Responses(String id, String questionId, List<String> answer, String recordedBy) {
        this.id = id;
        this.questionId = questionId;
        this.answer = answer;
        this.recordedBy = recordedBy;
        this.timestamp = new Date();
    }
    public Responses(String questionId, List<String> answer, String recordedBy) {
        this.id = id;
        this.questionId = questionId;
        this.answer = answer;
        this.recordedBy = recordedBy;
        this.timestamp = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<String> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getRecordedBy() {
        return recordedBy;
    }

    public void setRecordedBy(String recordedBy) {
        this.recordedBy = recordedBy;
    }
}
