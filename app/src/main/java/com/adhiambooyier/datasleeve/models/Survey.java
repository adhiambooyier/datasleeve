package com.adhiambooyier.datasleeve.models;

import java.util.Date;
import java.util.List;

public class Survey {
    private String id;
    private String title;
    private String description;
    private Date startDate;
    private Date endDate;
    private String createdBy;
    private String createdById;
    private String organizationId;
    private List<String> teamMembers;
    private int responseCount;


    public Survey(){
    }

    public Survey(String id, String title, String description, Date startDate, Date endDate, String createdBy, String createdById, String organizationId, List<String> teamMembers) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdBy = createdBy;
        this.createdById = createdById;
        this.organizationId = organizationId;
        this.teamMembers = teamMembers;
        responseCount = 0;
    }
    public Survey(String title, String description, Date startDate, String createdBy, String createdById, String organizationId, List<String> teamMembers) {
        this.createdById = createdById;
        this.organizationId = organizationId;
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.createdBy = createdBy;
        this.teamMembers = teamMembers;
        responseCount = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<String> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(List<String> teamMembers) {
        this.teamMembers = teamMembers;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public int getResponseCount() {
        return responseCount;
    }

    public void setResponseCount(int responseCount) {
        this.responseCount = responseCount;
    }
}
